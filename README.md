# Git Workshop/ Lecture/ Presentation

This repository holds my Git teaching material I use for workshops and lectures.

Basically that should be a presentation with an extra directory full of examples.

Usually all events get their own branch and once they have past, the branch is merged into [master](https://gitlab.isb-sib.ch/sbienert/Git-Workshop/tree/master). Old material will move into the `old/` directory. Branches of past events may vanish over time.

To get a copy of this repository locally, run

```
$ git clone https://gitlab.isb-sib.ch/sbienert/Git-Workshop.git
```

The whole thing is licensed by the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
