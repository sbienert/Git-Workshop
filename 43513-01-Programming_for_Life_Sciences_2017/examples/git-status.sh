#!/bin/bash

TESTREPO=test-git-status

# show 'no changes'
# show tracked files with changes

# create example repo
git init $TESTREPO
cd $TESTREPO

# show content
ls -a

# show empty
echo "###### OUTPUT FOR EMPTY DIR ######"
git status

# add a file
echo "*.pyc" >> .gitignore

# show untracked
echo "###### UNTRACKED ######"
git status

# add file to repo
git add .gitignore

# show tracked
echo "###### TRACKED ######"
git status

# commit
git commit -m 'Adding .gitignore'

# show after commit
echo "###### COMMITED ######"
git status

# clean up
cd ..
rm -rf $TESTREPO
