#!/bin/bash

TESTREPO=test-git-add

# create example repo
git init $TESTREPO
cd $TESTREPO

# show empty
echo "###### OUTPUT FOR EMPTY DIR ######"
git status

# add a file & show untracked
echo "*.pyc" >> .gitignore
echo "######       UNTRACKED      ######"
git status

# add file to repo & show tracked
git add .gitignore
echo "######        TRACKED       ######"
git status

# commit & show status
echo "######       COMMITED       ######"
git commit -m 'Adding .gitignore'
git status

# modify file & show status
echo "*.c" >> .gitignore
echo "######       MODIFIED       ######"
git status


# clean up
cd ..
rm -rf $TESTREPO
