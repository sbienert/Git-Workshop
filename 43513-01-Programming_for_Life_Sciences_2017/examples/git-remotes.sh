#!/bin/bash

TESTREPO=test-git-remotes

# create example repo with content (A)
# create an empty bare repo (B)
# hook it up with (A)
# push everything to (A)
# clone the bare repo and show that everything is in (C)

# CREATE THE SOURCE
# (e.g. your current project)
echo "###### CREATE SOURCE REPO  ######"
git init ${TESTREPO}-A
cd ${TESTREPO}-A

# content
echo "###### CREATE CONTENT ######"
echo "*.pyc" >> .gitignore
git add .gitignore
git commit -m 'Adding .gitignore'
echo "#include <stdin.h>" >> main.c
git add main.c
git commit -m 'Adding main'
echo "#include <stdio.h>" >> main.c
git commit -a -m 'More input/ output for main'

# CREATE BARE REPO
echo "###### CREATE DEST/ CENTRAL/ BARE REPO  ######"
cd ..
git init --bare ${TESTREPO}-B

# PUSH TO BARE REPO
echo "###### ADD BARE REPO TO EXISTING PROJECT ######"
cd ${TESTREPO}-A
git remote add origin ../${TESTREPO}-B
echo "###### PUSH TO CENTRAL HUB ######"
git push --set-upstream origin master

# CREATE NEW CLONE
echo "###### CLONE FROM BARE REPO ######"
cd ..
git clone ${TESTREPO}-B ${TESTREPO}-C
cd ${TESTREPO}-C
ls -a
git log

# clean up
cd ..
rm -rf ${TESTREPO}-A
rm -rf ${TESTREPO}-B
rm -rf ${TESTREPO}-C
