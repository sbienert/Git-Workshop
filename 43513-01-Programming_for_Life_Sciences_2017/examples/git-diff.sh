#!/bin/bash

TESTREPO=test-git-diff

# create example repo
git init $TESTREPO
cd $TESTREPO

# add a file
echo "*.pyc" >> .gitignore

# do commit so we can compare
git add .gitignore
git commit -m 'Adding .gitignore'

# change file and show diff
echo "*.c" >> .gitignore
git diff HEAD~0 .gitignore

# clean up
cd ..
rm -rf $TESTREPO
