#!/bin/bash

TESTREPO=test-git-init

## new project
# create example repo
git init $TESTREPO
cd $TESTREPO

# show whats in there
git status

# populate, .gitignore first
touch .gitignore
git status
git add .gitignore
git status
git commit -m 'Added .gitignore'
git status

# clean up
cd ..
rm -rf $TESTREPO

echo "#################################################################"
## existing project
# prepare
mkdir $TESTREPO
cd $TESTREPO
mkdir src
touch src/main.c
touch src/main.h
touch README
ls

# create example repo
git init

# show whats in there
git status

# populate
git add src
git status
git add README
git status
git commit -m 'Adding current state'
git status

# clean up
cd ..
rm -rf $TESTREPO
