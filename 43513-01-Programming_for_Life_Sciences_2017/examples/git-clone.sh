#!/bin/bash

TESTREPO=test-git-clone

# clone repo (A) from a GitLab server

# CREATE A CLONE
echo "###### CLONE REPO FROM GitLab@SIB######"
git clone https://gitlab.isb-sib.ch/sbienert/Git-Workshop.git ${TESTREPO}
cd ${TESTREPO}
ls -a
git branch -a

# clean up
cd ..
rm -rf ${TESTREPO}
