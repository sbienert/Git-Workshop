#!/bin/bash

TESTREPO=test-git-log

# create example repo
git init $TESTREPO
cd $TESTREPO

# add a file
echo "*.pyc" >> .gitignore

# do commit so we can compare
git add .gitignore
git commit -m 'Adding .gitignore'

# change file and commit
echo "*.o" >> .gitignore
git commit -a -m 'Ignoring object files'

# show history
echo "###### SHOW HISTORY ######"
git log

# clean up
cd ..
rm -rf $TESTREPO
