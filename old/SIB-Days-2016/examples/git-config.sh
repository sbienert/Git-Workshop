#!/bin/bash

TESTREPO=test-git-config

# set user/ mail
git config --global user.name "Stefan Bienert"
git config --global user.email "Stefan.Bienert@sib.swiss"

# set editor
git config --global core.editor "emacs"

# set colour
git config --global color.ui "auto"

# show config
git config --list

# create example repo
mkdir $TESTREPO
cd $TESTREPO
git init

# populate with file
touch thesis.tex
git add thesis.tex

# commit
git commit -m 'Started masterpiece'
git status

# commit more
echo "Title 'Attack of the mutant suffix arrays'" >> thesis.tex
git commit -a -m 'Set title'

# switch user/ mail
git config --global user.name "Ron Appel"
git config --global user.email "Ron@App.el"

# commit
echo "Abstract 'I.O.U. an abstract'" >> thesis.tex
git commit -a -m 'Placeholder for abstract'

# switch user/ mail
git config --global user.name "Monica Pozzato"
git config --global user.email "Monica@Pozza.to"

# commit
git mv thesis.tex thesis.txt
git commit -a -m "This ain't no LaTeX!"

# show log/ blame
git log --follow thesis.txt
git blame thesis.txt

# clean up
cd ..
rm -rf $TESTREPO

# set user/ mail
git config --global user.name "Stefan Bienert"
git config --global user.email "stefan.bienert@unibas.ch"
