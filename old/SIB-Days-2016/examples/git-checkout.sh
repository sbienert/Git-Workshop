#!/bin/bash

TESTREPO=test-git-checkout

# create example repo
git init $TESTREPO
cd $TESTREPO

# add a file
echo "*.pyc" >> .gitignore
git add .gitignore
git commit -m 'Adding .gitignore'

# change file and commit to get a history
echo "*.c" >> .gitignore
git commit -a -m 'Ignoring C source files'
echo "*.o" >> .gitignore
git commit -a -m 'Ignoring object files'
echo "*.tex" >> .gitignore
git commit -a -m 'Ignoring LaTeX files'

# show history
echo "###### SHOW HISTORY ######"
git log

# show current file
echo "###### .gitignore ######"
cat .gitignore

# ups... of course we want to track LaTeX
echo "###### 1 COMMIT UNDONE ######"
git checkout HEAD~1 .gitignore
cat .gitignore

# ups again... we do not want to hide C sources
echo "###### 2 COMMITS UNDONE ######"
git checkout HEAD~3 .gitignore
cat .gitignore

# reverting changes does not delete commits
echo "###### HISTORY ######"
git log

# undo changes
echo "###### UNDO ######"
git status
git reset HEAD .gitignore
git checkout -- .gitignore
cat .gitignore

# clean up
cd ..
rm -rf $TESTREPO

