#!/bin/bash

TESTREPO=test-git-history

# create 'project directory'
mkdir $TESTREPO
cd $TESTREPO

# create Python virtual environment
virtualenv venv

# activate Python virtual environment
source venv/bin/activate

# upgrade PIP if necessary
pip install --upgrade pip

# install matplotlib INSIDE the Python virtual environment
pip install matplotlib

# before starting to do coding, turn the project into a Git repository
git init

# a Python virtual environment is system specific, does not belong into a Git
# repo (use `pip freeze` for that)
echo 'venv/' >> .gitignore

# add .gitignore to the Git repo
git add .gitignore

# commit changes to the Git repo
git commit -m 'Started .gitignore'

# create Python script with simple plot
echo "import matplotlib.pyplot as plt" >> simple-plot.py
echo "plt.plot([1,2,3,4])" >> simple-plot.py
echo "plt.ylabel('some numbers')" >> simple-plot.py
echo "plt.show()" >> simple-plot.py

# run the script
python simple-plot.py

# add the script to the Git repo
git add simple-plot.py

# commit changes to the Git repo
git commit -m 'Started simple-plot.py'

# add proper units on the Y axis
ed -- "simple-plot.py" <<-EOF
    3s!some numbers')!occurrences (arb. unit)'!
    w
    q
EOF

# mark the changes to be committed to the Git repo
git add simple-plot.py

# commit changes to the Git repo
git commit -m 'Better Y axis labels in the plot'

# add units to the X axis
ed -- "simple-plot.py" <<-EOF
    3
    a
plt.xlabel('time (s)')
.
    w
    q
EOF

# mark the changes to be committed to the Git repo
git add simple-plot.py

# commit changes to the Git repo
git commit -m 'Better X axis labels in the plot'


# execute Python script again to see changes
python simple-plot.py

# Ooops, Python script is broken, lets see where we messed up...
git log

# go back in time, to a commit where the Python script was still working
git reset --hard HEAD~2

ed -- "simple-plot.py" <<-EOF
    3s!some numbers')!occurrences (arb. unit)')!
    w
    3
    a
plt.xlabel('time (s)')
.
    w
    q
EOF

python simple-plot.py

git add simple-plot.py

git commit -m 'Better X/Y axis labels in the plot'

# clean up
#cd ..
#rm -rf $TESTREPO
