#!/bin/bash

TESTREPO=test-git-branch

# create example repo
git init $TESTREPO
cd $TESTREPO

# add a file
echo "###### CREATE COMMIT ######"
echo "*.pyc" >> .gitignore
git add .gitignore
git commit -m 'Adding .gitignore'

# now create a new branch develop & switch to it
echo "###### BRANCHING ######"
git branch develop
git branch
echo "###### SWITCH TO develop ######"
git checkout develop
git branch

# change sth in the new branch
echo "###### CHANGE SOMETHING ######"
echo "*.o" >> .gitignore
git commit -a -m 'Ignore object files'

# show content
echo "###### .gitignore (develop) ######"
cat .gitignore

# switch back to master
echo "###### SWITCH TO master ######"
git checkout master

# show content
echo "###### .gitignore (master) ######"
cat .gitignore


# clean up
cd ..
rm -rf $TESTREPO

