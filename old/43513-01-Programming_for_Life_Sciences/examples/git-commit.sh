#!/bin/bash

TESTREPO=test-git-commit

# create example repo
git init $TESTREPO
cd $TESTREPO

# add a file
echo "*.pyc" >> .gitignore

# try to commit without 'git add'
echo "######     COMMIT WO ADD    ######"
git commit -m 'Adding .gitignore'

# add file to repo & commit
echo "######        COMMIT        ######"
git add .gitignore
git commit -m 'Adding .gitignore'

# clean up
cd ..
rm -rf $TESTREPO
