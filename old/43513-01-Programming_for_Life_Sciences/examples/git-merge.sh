#!/bin/bash

TESTREPO=test-git-merge

# create example repo
git init $TESTREPO
cd $TESTREPO

# add a file
echo "###### CREATE .gitignore ######"
echo "*.pyc" >> .gitignore
git add .gitignore
git commit -m 'Adding .gitignore'

# now create a new branch develop & switch to it
echo "###### CREATE & SWITCH develop ######"
git checkout -b develop

# change sth in the new branch
echo "###### CHANGE SOMETHING ######"
echo "*.o" >> .gitignore
git commit -a -m 'Ignore object files'

# switch back to master
echo "###### SWITCH TO master ######"
git checkout master

# show content
echo "###### .gitignore (master) ######"
cat .gitignore

# merge develop
echo "###### MERGE develop INTO master ######"
git merge develop
cat .gitignore

### go for a merge conflict
# change in master
echo "*.log" >> .gitignore
git commit -a -m 'Ignore log files'

git checkout develop
echo "*.mo" >> .gitignore
git commit -a -m 'Ignore qmake objects'

# merge master into develop
echo "###### UPDATE develop ######"
git merge master

echo "###### STATUS ######"
git status

echo "###### .gitignore ######"
cat .gitignore

# clean up
cd ..
rm -rf $TESTREPO

